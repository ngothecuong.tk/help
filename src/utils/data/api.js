import axios from 'axios';
// json-server --watch src/utils/data/data.json --port 3004
export const api = axios.create({
    baseURL: 'http://localhost:3004/',
});
