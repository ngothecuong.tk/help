import { toast } from 'react-toastify';
export const notifyWithError = (message) =>
    toast.error(message, {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
    });

export const notifyWithSuccess = (message) =>
    toast.success(message, {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
    });
