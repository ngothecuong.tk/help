import React, { useState, useEffect } from 'react';
import { Paper, Box, Typography, Divider, Avatar, ListItem, List } from '@mui/material';
import { api } from '@utils/data/api';

const CalenderSideBar = ({ user, status, userId }) => {
    return (
        <Box>
            <Box className="pages_calendar_side_bar_header">
                <Typography className="pages_calendar_side_bar_header_title" variant="p">
                    TASK MANAGER
                </Typography>
            </Box>
            <Divider />
            <Box className="pages_calendar_side_bar_info">
                <Avatar alt={user.employeeName} src="/static/images/avatar/1.jpg" sx={{ width: 100, height: 100 }} />
                <Typography className="pages_calendar_side_bar_info_name" variant="p">
                    {user.employeeName}
                </Typography>
                <Typography className="pages_calendar_side_bar_info_email" variant="p">
                    {user.email}
                </Typography>
            </Box>
            <Divider />
            <Box className="pages_calendar_side_bar_status">
                <Typography className="" variant="p">
                    STATUS
                </Typography>
                <List sx={{ width: '100%', maxWidth: 360 }}>
                    <ListItem disableGutters>
                        <Box className="pages_calendar_side_bar_status_color pages_calendar_side_bar_status_todo"></Box>
                        <Typography className="pages_calendar_side_bar_status_title" variant="p">
                            To do ({status.to_do})
                        </Typography>
                    </ListItem>
                    <ListItem disableGutters>
                        <Box className="pages_calendar_side_bar_status_color pages_calendar_side_bar_status_urgent"></Box>
                        <Typography className="pages_calendar_side_bar_status_title" variant="p">
                            Urgent ({status.urgent})
                        </Typography>
                    </ListItem>
                    <ListItem disableGutters>
                        <Box className="pages_calendar_side_bar_status_color pages_calendar_side_bar_status_done"></Box>
                        <Typography className="pages_calendar_side_bar_status_title" variant="p">
                            Done ({status.done})
                        </Typography>
                    </ListItem>
                </List>
            </Box>
            <Divider />
            <Box className="pages_calendar_side_bar_team">
                <Typography className="" variant="p">
                    TEAM 2
                </Typography>
                <Box className="pages_calendar_side_bar_team_members">
                    <Avatar alt="Bao" src="/static/images/avatar/1.jpg" />
                    <Avatar alt="Hieu" src="/static/images/avatar/2.jpg" />
                    <Avatar alt="Cuong" src="/static/images/avatar/3.jpg" />
                </Box>
            </Box>
        </Box>
    );
};

export default CalenderSideBar;
