import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Paper, Snackbar } from '@mui/material';
import { EditingState, ViewState } from '@devexpress/dx-react-scheduler';
import { green, red } from '@mui/material/colors';
import { api } from '@utils/data/api';
import { notifyWithError } from '@utils/helpers/notify';
import { ToastContainer } from 'react-toastify';
import {
    Scheduler,
    WeekView,
    Appointments,
    DragDropProvider,
    EditRecurrenceMenu,
    CurrentTimeIndicator,
    AppointmentTooltip,
    AppointmentForm,
    DateNavigator,
    Toolbar,
    TodayButton,
    Resources,
    DayView,
    ViewSwitcher,
} from '@devexpress/dx-react-scheduler-material-ui';

const CalenderBoard = ({ handleAppointment, appointments, handleStatus, userId }) => {
    const [resources, setResources] = useState([
        {
            fieldName: 'progress',
            title: 'Progress',
            instances: [
                {
                    text: 'Urgent',
                    id: 1,
                    color: red,
                },
                {
                    text: 'Done',
                    id: 2,
                    color: green,
                },
            ],
        },
    ]);

    useEffect(() => {
        const urgent = appointments.filter((appointment) => appointment.progress == 1).length;
        const done = appointments.filter((appointment) => appointment.progress == 2).length;
        const to_do = appointments.filter((appointment) => !('progress' in appointment)).length;
        const status = {
            to_do: to_do,
            done: done,
            urgent: urgent,
        };
        handleStatus(status);
    }, [appointments]);

    const commitChanges = (props) => {
        if (props.added) {
            let checkDuplicate = appointments.some((element) => {
                let compare = compareDate(element.startDate, element.endDate, props.added.startDate, props.added.endDate);
                if (compare === true) {
                    return true;
                }
            });
            if (props.added.endDate.getTime() < props.added.startDate.getTime()) {
                notifyWithError('Time Error!');
                return;
            } else if (checkDuplicate) {
                notifyWithError("Don't multitasking !");
                return;
            } else {
                api.post('tasks', { ...props.added, employeeId: userId }).then((res) => {
                    let dataAdded = [...appointments, res.data];
                    handleAppointment(dataAdded);
                });
            }
        }

        if (props.changed) {
            const starDate = props?.changed[Object.keys(props.changed)[0]]?.startDate;
            const endDate = props?.changed[Object.keys(props.changed)[0]]?.endDate;
            if (typeof starDate !== 'undefined' ? starDate : 0 < typeof endDate !== 'undefined' ? endDate : 0) {
                notifyWithError('Time Error!');
                return;
            }

            let data = appointments.map((appointment) =>
                props.changed[appointment.id] ? { ...appointment, ...props.changed[appointment.id] } : appointment,
            );
            let checkDuplicate = appointments.some((element) => {
                if (Object.keys(props?.changed)[0] != element.id) {
                    let compare = compareDate(
                        element.startDate,
                        element.endDate,
                        props?.changed[Object.keys(props.changed)[0]].startDate,
                        props?.changed[Object.keys(props.changed)[0]].endDate,
                    );
                    if (compare === true) {
                        return true;
                    }
                }
            });
            if (checkDuplicate) {
                notifyWithError("Don't multitasking !");
                return;
            }
            api.patch(`tasks/${Object.keys(props.changed)[0]}`, props.changed[Object.keys(props.changed)[0]]);
            handleAppointment(data);
        }

        if (props.deleted !== undefined) {
            let data = appointments.filter((appointment) => appointment.id !== props.deleted);
            api.delete(`tasks/${props.deleted}`);
            handleAppointment(data);
        }
    };

    const compareDate = (startDate, endDate, startDropDate, endDropDate) => {
        const startPrevDate = new Date(startDate).getTime();
        const endPrevDate = new Date(endDate).getTime();
        const startNextDate = new Date(startDropDate).getTime();
        const endNextDate = new Date(endDropDate).getTime();

        if (
            Math.min(startPrevDate, endPrevDate) < Math.max(startNextDate, endNextDate) &&
            Math.max(startPrevDate, endPrevDate) > Math.min(startNextDate, endNextDate)
        ) {
            return true;
        } else {
            return false;
        }
    };

    return (
        <Paper className="pages_calendar_box_shadow">
            <Scheduler data={appointments} height={660}>
                <ViewState defaultCurrentViewName="Week" />
                <EditingState onCommitChanges={commitChanges} />
                <EditRecurrenceMenu />
                <WeekView startDayHour={8.5} endDayHour={17.5} />
                <DayView startDayHour={8.5} endDayHour={17.5} />
                <Toolbar />
                <DateNavigator />
                <TodayButton />
                <Appointments />
                <AppointmentTooltip showOpenButton showDeleteButton />
                <AppointmentForm />
                <Resources data={resources} mainResourceName="progress" />
                <DragDropProvider />
                <CurrentTimeIndicator shadePreviousCells={true} />
                <ViewSwitcher />
            </Scheduler>
            <ToastContainer autoClose={1500} />
        </Paper>
    );
};

export default CalenderBoard;
