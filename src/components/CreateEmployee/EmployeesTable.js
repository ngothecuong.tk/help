import React, { useState, useEffect } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    Checkbox,
    TableBody,
    TableFooter,
    TablePagination,
    Button,
} from '@mui/material';
import { Link } from 'react-router-dom';

const rowHeaders = ['ID', 'Employee Name', 'Email', 'Age', 'Phone', 'Position', 'Option'];
const EmployeesTable = ({ onPageChange, pagination, handleChecked, users, getEmployee, handleDelete }) => {

    console.log(pagination);
   
    const checkedAll = users.filter((user) => user.status !== true);
    const isCheck = users.find((user) => user.status);
    const [rowsPerPage, setRowsPerPage] = useState(pagination.limit);
    const [page, setPage] = useState(0);

    useEffect(() => {
        setPage(pagination.page - 1);
    }, [pagination, users]);


    const handleChange = (e) => {
        const setChecked = users.map((user) => (e.target.id == user.id ? { ...user, status: e.target.checked } : user));
        handleChecked(setChecked);
    };

    const handleCheckAll = (e) => {
        const setChecked = users.map((user) => {
            return { ...user, status: e.target.checked };
        });
        handleChecked(setChecked);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        onPageChange(newPage + 1);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <Checkbox checked={checkedAll.length < 1 && typeof isCheck !== 'undefined'} onChange={handleCheckAll} />
                        </TableCell>
                        {rowHeaders.map((header, index) => {
                            return <TableCell key={index}>{header}</TableCell>;
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((user) => (
                        <TableRow key={user.id}>
                            <TableCell>
                                <Checkbox checked={user.status || false} id={user.id} onChange={handleChange} />
                            </TableCell>
                            <TableCell>{user.id}</TableCell>
                            <TableCell>
                                <Link to={`/calendar/${user.id}`}>
                                    <Button>{user.employeeName}</Button>
                                </Link>
                            </TableCell>
                            <TableCell>{user.email}</TableCell>
                            <TableCell>{user.age}</TableCell>
                            <TableCell>{user.phone}</TableCell>
                            <TableCell>{user.position}</TableCell>
                            <TableCell>
                                <Button className="pages_create_employees_button" onClick={() => getEmployee(user.id)} variant="contained">
                                    EDIT
                                </Button>
                                <Button
                                    userId={user.index}
                                    className="pages_create_employees_button"
                                    onClick={() => handleDelete(user.id)}
                                    variant="contained"
                                    color="error">
                                    DELETE
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10]}
                            rowsPerPage={rowsPerPage}
                            count={pagination.totalRows}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

export default EmployeesTable;
