import React, { useState } from 'react';
import { Modal, Box, TextField, Button } from '@mui/material';
import validator from 'validator';
import isEmail from 'validator/lib/isEmail';

const ModalData = ({ handleClose, modalOpen, handleSubmit, formData, handleChange }) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        //bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        borderRadius: '5px',
    };

    const [rules, setRule] = useState([
        {
            field: 'employeeName',
            label: 'Employee Name',
            type: 'text',
            name: 'employeeName',
            default_value: formData?.employeeName,
        },
        {
            field: 'email',
            label: 'Email',
            type: 'text',
            name: 'email',
            default_value: formData?.email,
        },
        {
            field: 'age',
            label: 'Age',
            type: 'number',
            name: 'age',
            default_value: formData?.age,
        },
        {
            field: 'position',
            label: 'Position',
            type: 'text',
            name: 'postion',
            default_value: formData?.position,
        },
    ]);

    const handleOnChangeForm = (e) => {
        const valid = rules.map((item) => (item.field === e.target.name ? { ...item, isValid: validator.isEmail(e.target.value) } : item));
        setRule(valid);
        if (handleChange) {
            handleChange(e);
        }
    };

    return (
        <Modal open={modalOpen} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
            <Box className="components__modal_box">
                <form onSubmit={handleSubmit}>
                    <TextField
                        defaultValue={formData?.employeeName}
                        sx={{ mb: 2 }}
                        onChange={handleOnChangeForm}
                        fullWidth
                        name="employeeName"
                        label="Employee Name"
                        type="text"
                    />
                    <TextField
                        defaultValue={formData?.email}
                        sx={{ mb: 2 }}
                        onChange={handleOnChangeForm}
                        fullWidth
                        name="email"
                        label="Email"
                        type="text"
                    />
                    <TextField
                        defaultValue={formData?.age}
                        sx={{ mb: 2 }}
                        onChange={handleOnChangeForm}
                        fullWidth
                        name="age"
                        label="Age"
                        type="number"
                    />
                    <TextField
                        defaultValue={formData?.phone}
                        sx={{ mb: 2 }}
                        onChange={handleOnChangeForm}
                        fullWidth
                        name="phone"
                        label="Phone"
                        type="number"
                    />
                    <TextField
                        defaultValue={formData?.position}
                        sx={{ mb: 2 }}
                        onChange={handleOnChangeForm}
                        fullWidth
                        name="position"
                        label="Position"
                        type="text"
                    />
                    <Button type="submit" className="pages_create_employees_button" variant="contained">
                        {formData?.type == 'edit' ? 'UPDATE' : 'ADD'}
                    </Button>
                    <Button onClick={handleClose} variant="contained">
                        Close
                    </Button>
                </form>
            </Box>
        </Modal>
    );
};

export default ModalData;
