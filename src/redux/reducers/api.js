import { SET_LOADER } from '../actions/type';

const loaderReducer = (state = true, action) => {
    switch (action.type) {
        case SET_LOADER:
            return action.data;
        default:
            return state;
    }
};

export { loaderReducer };
