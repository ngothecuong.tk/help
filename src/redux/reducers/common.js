import { SET_LOCALE } from '../actions/type';

const localeReducer = (state = 'jp', action) => {
    switch (action.type) {
        case SET_LOCALE:
            return action.data;
        default:
            return state;
    }
};

export { localeReducer };
