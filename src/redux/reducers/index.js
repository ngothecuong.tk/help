import { combineReducers } from 'redux';
import { loaderReducer } from './api';
import { localeReducer } from './common';

const rootReducers = combineReducers({
    loader: loaderReducer,
    locale: localeReducer,
});

export default rootReducers;
