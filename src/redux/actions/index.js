import { fetchLogin } from './api';
import { setLocale } from './common';
import { setLoader } from './api';

export { fetchLogin, setLocale, setLoader };
