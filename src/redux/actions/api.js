import { SET_LOADER } from './type';

export const setLoader = (data = false) => {
    return {
        type: SET_LOADER,
        data,
    };
};

export const fetchLogin = async (data = {}, callBack = () => {}) => {
    return async (dispatch) => {
        dispatch(setLoader(true));

        // await axios.post

        setTimeout(() => {
            dispatch(setLoader(false));
            const res = data;
            callBack(res);
        }, 1000);
    };
};
