import { SET_LOCALE } from './type';

// Action set active locale
export const setLocale = (data = 'jp') => {
    return {
        type: SET_LOCALE,
        data,
    };
};
