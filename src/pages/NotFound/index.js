import React from 'react';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import '@scss';

const NotFound = () => {
    return (
        <div className="pages_notfound">
            NOT FOUND
            <Button>
                <Link to="/">Back</Link>
            </Button>
        </div>
    );
};

export default NotFound;
