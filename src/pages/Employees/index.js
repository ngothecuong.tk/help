import React from 'react';
import { useEffect, useState } from 'react';
import { api } from '@utils/data/api';
import CheckList from '../../components/Employess/showListCheck';
import { Header } from '../../components/Employess/index';

const Employee = () => {
    const [subject, setSubject] = useState([]);
    const [userEdit, setUserEdit] = useState([]);

    useEffect(() => {
        api.get(`/employees`).then((res) => {
            setSubject(res.data);
        });
    }, []);

    const checkValue = (arrData) => {
        return arrData.filter((arr) => arr?.isChecked !== true).length < 1;
    };

    const handleAddUser = (userInput) => {
        api.post('/employees', userInput);
        setSubject(userInput);
    };

    const handleEditUser = (user) => {
        setUserEdit(user);
    };
    const handleDeleteAll = (user) => {};

    const changeCheckBox = (event) => {
        const data = event.target;

        if (data.name === 'all') {
            let subjectItem = subject.map((subject) => {
                return { ...subject, isChecked: data.checked };
            });
            setSubject(subjectItem);
        } else {
            let subjectItem = subject.map((subject) => {
                if (subject.name === data.name) {
                    return { ...subject, isChecked: data.checked };
                } else {
                    return subject;
                }
            });
            setSubject(subjectItem);
        }
    };

    return (
        <>
            <Header userEdit={userEdit} handleAddUser={handleAddUser}></Header>
            <div className="row justify-content-center">
                <div className="col-sm-12">
                    <table className="table table-striped ">
                        <thead>
                            <tr>
                                <th>
                                    <input
                                        type="checkbox"
                                        name="all"
                                        defaultValue="all"
                                        onChange={changeCheckBox}
                                        checked={checkValue(subject)}
                                    />
                                </th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Position</th>
                                <th>Phone</th>
                                <th>Age</th>
                                <th>Handle</th>
                            </tr>
                        </thead>
                        <CheckList
                            handleEditUser={handleEditUser}
                            listData={subject}
                            changeCheckBox={changeCheckBox}
                            handleDeleteAll={handleDeleteAll}
                        />
                    </table>
                </div>
            </div>
        </>
    );
};
export default Employee;
