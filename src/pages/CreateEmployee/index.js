import React, { useState, useEffect } from 'react';
import { Paper, Button, Box } from '@mui/material';
import { api } from '@utils/data/api';
import ModalData from '../../components/CreateEmployee/ModalData';
import EmployeesTable from '../../components/CreateEmployee/EmployeesTable';
import { Loading } from '../';
import { notifyWithSuccess } from '@utils/helpers/notify';
import { ToastContainer } from 'react-toastify';
import queryString from 'query-string';

const CreateEmployee = () => {
    const [users, setUsers] = useState([]);
    const [formData, setFormData] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);
    const [loading, setLoading] = useState(true);

    const [pagination, setPagination] = useState({
        page: 1,
        limit: 5,
        totalRows: 1,
        pageLimit: 5,
    });

    const [filters, setFilters] = useState({
        _limit: 5,
        _page: 1,
    });

    useEffect(() => {
        api.get(`/employees`).then((res) => {
            // setUsers(res.data);
            setPagination({
                ...pagination,
                totalRows: res.data.length,
            });
        });
    }, [users]);

    useEffect(() => {
        const paramsString = queryString.stringify(filters);
        api.get(`/employees?${paramsString}`).then((res) => {
            setUsers(res.data);
            setLoading(false);
        });
    }, [filters]);

    const onPageChange = (newPage) => {
        setPagination({ ...pagination, page: newPage });
        setFilters({ ...filters, _page: newPage });
    };

    const checkedAll = users.filter((user) => user.status === true);

    const handleSubmit = (event) => {
        event.preventDefault();
        if (formData?.type === 'edit') {
            const data = users.map((user, index) => (user.id === formData.id ? (users[index] = formData) : user));
            api.put(`/employees/${formData.id}`, formData).then((res) => {
                setUsers(data);
                setModalOpen(false);
                notifyWithSuccess('Updated successfully !');
            });
        } else {
            api.post('/employees', formData).then((res) => {
                setUsers([...users, res.data]);
                setModalOpen(false);
                notifyWithSuccess('Added successfully !');
            });
            if (filters._limit == users.length) {
                setFilters({ ...filters, _page: filters._page + 1 });
                setPagination({ ...pagination, page: pagination.page + 1 });
            }
        }
        setFormData(null);
    };

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleDelete = (id) => {
        let data = users.filter((user) => user.id !== id);
        api.delete(`/employees/${id}`).then((res) => {
            setUsers(data);
            notifyWithSuccess('Deleted successfully !');
        });
        console.log(users.length);
        console.log(users.length <= filters._limit);
        if (users.length == 1 || users.length == filters._limit) {
            setFilters({ ...filters, _page: filters._page - 1 == 0 ? filters._page : filters._page - 1 });
            setPagination({ ...pagination, page: pagination.page - 1 == 0 ? pagination.page : pagination.page - 1 });
        }
    };

    const getEmployee = (id) => {
        let dataEmployeeObj = {};
        const dataEmployee = users.filter((user) => user.id === id);
        dataEmployeeObj = dataEmployee[0];
        setFormData({ ...dataEmployeeObj, type: 'edit' });
        setModalOpen(true);
    };

    const handleClose = () => {
        setFormData(null);
        setModalOpen(false);
    };

    const handleChecked = (checked) => {
        setUsers(checked);
    };

    const handleDeleteAll = () => {
        const idDelete = users
            .filter((user) => user.status === true)
            .map((item) => {
                return item.id;
            });
        let data = users.filter((user) => !idDelete.includes(user.id));
        idDelete.forEach((id) => {
            api.delete(`/employees/${id}`);
        });
        setUsers(data);
        notifyWithSuccess('Deleted successfully !');
        setFilters({ ...filters, _page: filters._page - 1 == 0 ? filters._page : filters._page - 1 });
        setPagination({ ...pagination, page: pagination.page - 1 == 0 ? pagination.page : pagination.page - 1 });
    };

    return (
        <div className="pages__employees">
            {loading ? (
                <Loading />
            ) : (
                <Paper>
                    <div className="menu">

                        <div className="content">
                            <h2>
                                Manage <b>Employees</b>
                            </h2>
                        </div>
                        <div className="button_group">
                            <div className="button">
                                <Button onClick={() => setModalOpen(true)} sx={{ mb: 1, mr: 1 }} variant="contained">
                                    ADD EMPLOYEE
                                </Button>
                                <Button disabled={checkedAll.length < 1} onClick={handleDeleteAll} sx={{ mb: 1 }} variant="contained" color="error">
                                    DELETE
                                </Button>
                            </div>
                        </div>

                    </div>

                    <EmployeesTable
                        onPageChange={onPageChange}
                        pagination={pagination}
                        handleChecked={handleChecked}
                        users={users}
                        getEmployee={getEmployee}
                        handleDelete={handleDelete}
                    />
                    <ModalData
                        handleClose={handleClose}
                        modalOpen={modalOpen}
                        handleSubmit={handleSubmit}
                        formData={formData}
                        handleChange={handleChange}
                    />
                    <ToastContainer autoClose={1000} />
                </Paper>
            )}
        </div>
    );
};

export default CreateEmployee;
