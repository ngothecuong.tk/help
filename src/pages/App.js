import { Routes, Route } from 'react-router-dom';

import { Calendar, NotFound, CreateEmployee } from '@Pages';

function App() {
    return (
        <Routes>
            <Route path="/" element={<CreateEmployee />} />
            <Route path="calendar/:id" element={<Calendar />} />
            <Route path="add" element={<CreateEmployee />} />
            <Route path="404" element={<NotFound />} />
            <Route path="*" element={<NotFound />} />
        </Routes>
    );
}

export default App;
