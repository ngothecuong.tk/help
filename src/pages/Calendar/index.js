import React, { useState, useEffect } from 'react';
import { CalendarBoard, CalendarSideBar } from '@Components';
import { Paper, Grid, Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { api } from '@utils/data/api';
import Loading from '../Loading';
import { useDispatch, useSelector } from 'react-redux';
import { setLoader } from '../../redux/actions';
import '@scss';

const Calendar = () => {
    const [status, setStatus] = useState([]);
    const [user, setUser] = useState([]);
    const [appointments, setAppointment] = useState([]);
    const userId = useParams().id;
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();
    const { loader } = useSelector((state) => state);

    useEffect(() => {
        const getUser = api.get(`/employees/${userId}`);
        const getCalendar = api.get(`/tasks?employeeId=${userId}`);
        Promise.all([getUser, getCalendar]).then((requestData) => {
            setUser(requestData[0].data);
            setAppointment(requestData[1].data);
            setLoading(false);
        });
    }, []);

    const handleStatus = (status) => {
        setStatus(status);
    };

    const handleAppointment = (data) => {
        setAppointment(data);
    };

    return (
        <>
            {loading ? (
                <Loading />
            ) : (
                <Paper container>
                    <Grid className="pages_calendar" container>
                        <Grid sx={{ height: '100%', pl: 3 }} item xs={3}>
                            <CalendarSideBar user={user} userId={userId} status={status} />
                        </Grid>
                        <Grid item xs={9} sx={{ pr: 3, pl: 3 }}>
                            <Link to="/">
                                <Button sx={{ mb: 1, mt: 1 }} variant="contained">
                                    Employees Board
                                </Button>
                            </Link>
                            <CalendarBoard
                                handleAppointment={handleAppointment}
                                appointments={appointments}
                                userId={userId}
                                handleStatus={handleStatus}
                            />
                        </Grid>
                    </Grid>
                </Paper>
            )}
        </>
    );
};

export default Calendar;
