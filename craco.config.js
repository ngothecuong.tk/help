/* craco.config.js */
const path = require(`path`);

module.exports = {
    webpack: {
        alias: {
            '@': path.resolve(__dirname, 'src/'),
            '@Components': path.resolve(__dirname, 'src/components'),
            '@Pages': path.resolve(__dirname, 'src/pages'),
            '@scss': path.resolve(__dirname, 'src/assets/styles/scss/main.scss'),
            '@utils': path.resolve(__dirname, 'src/utils'),
            '@api': path.resolve(__dirname, 'src/utils/data/api.js'),
        },
    },
};
